const models = require('../models');
const User = models.User;
const passport= require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
  ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'azerty';

const jwtAuthStrategy = passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
  User.findByPk(jwt_payload.id)
    .then((user) => {
      if (user) {
        return done(null,user)
      }else {
        return done(null,user)
      }
    })
    .catch((err) =>  {
      return done(err,false)
    })
}));


module.exports.jwtAuthStrategy = jwtAuthStrategy
