var express = require('express');
var router = express.Router();
const passport = require('passport')
const userControllers = require('../controllers/userControllers')
router.get('/',passport.authenticate('jwt',{session:false}),userControllers.listAll);
router.post('/',userControllers.create);
router.post('/signin',userControllers.signUser);
module.exports = router;
