const models = require('../models')
const User = models.User;
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');


const comparePassword = (req,res,user) => {
  return bcrypt.compare(req.body.password, user.password)
    .then((succes) => {
      if (succes) {
        const token = jwt.sign( user.toJSON(), 'azerty',{ expiresIn: '1h' });
        res.status(200).json({ message: "User connected",token })
      }else {
        res.status(401).json({ message: "Invalid credentials" })
      }
    })
    .catch((err) => console.log(err))
};


module.exports = {
  //list All Users
  listAll: (req, res) => {
    User.findAll(
      {
        attributes: ['id', 'username', 'lastname', 'firstname', 'email', 'country', 'isConnected'],
      })
      .then((users) => {
        if (users) {
          res.status(200).json(users);
        }
        else {
          res.status(404).json({"error": "users found"});
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).json({"error": err});
      })
  },

  //create Users
  create: (req, res) => {
    User.create({
      avatar: req.body.avatar,
      username: req.body.username,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      password: req.body.password,
      country: req.body.country,
      isConnected: req.body.isConnected || false,
      isAdmin: req.body.isAdmin || false,
    })
      .then((newUser) => res.status(200).json({newUser}))
      .catch((err) => res.status(500).json(err.errors.map(x => x.message).toString()))
  },

  signUser: (req, res) => {
      User.findOne({
        where:{
          email:req.body.email
        }
      }).then((user) => {
        if (user) {
          comparePassword(req,res,user)
        } else {
          res.status(404).json({ messsages: 'User not Found' })
        }
      })
        .catch((err) => res.json({err}))
  }
}
