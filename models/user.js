'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    avatar: DataTypes.STRING,
    username: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args:[3,20],
          msg:"the username must have a minimum of 3 characters and 20 characters maximum"
        }
      }
    },
    firstname: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args:[3,20],
          msg:"the firstname must have a minimum of 3 characters and 20 characters maximum"
        }
      }
    },
    lastname: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args:[3,20],
          msg:"the lastname must have a minimum of 3 characters and 20 characters maximum"
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {
          msg: "Email invalide"
        }
      },
    },

    password: {
      type: DataTypes.STRING,
      validate: {
        customValidator(password) {
          if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{4,20}$/g.test(password)) {
            throw new Error("pour des questions de sécurité le mot doit de passe doit etre compris entre 4 et 15 caracteres 1 caratéres special 1 nombre obligatoire un chiffre majuscule");
          }
        },
      }
    },
    country: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'Please enter your country'
        },
      }
    },
    isAdmin: DataTypes.BOOLEAN,
    isConnected: DataTypes.BOOLEAN
  }, {
    hooks:{
      beforeCreate: (user) => {
        return bcrypt.hash(user.password,10)
          .then((hash) => user.password = hash)
          .catch((err) => console.log(err))
      }
    }
  });
  User.associate = function (models) {
    // associations can be defined here
  };
  return User;
};
